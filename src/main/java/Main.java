public class Main {

    public static void main(String[] args) {
        int[] maListe = {1, 6, 4, 2, 9, 5, 3, 7, 8};
//        int[] maListe = {1, 2, 3, 4, 5, 6, 7, 9 ,8};

        boolean change = true;

        // Boucle dans la liste
        while (change) {
            change = false;
            for (int i = 0; i < maListe.length - 1; i++) {
                // Obtenir mes deux nombres
                int element_1 = maListe[i];
                int element_2 = maListe[i + 1];

                // Est-ce qu'on doit les changer de place?
                if (element_2 < element_1) {
                    // Échanger de place
                    maListe[i] = element_2;
                    maListe[i + 1] = element_1;

                    // Indiquer le changement
                    change = true;
                }
            }
        }

        // Afficher la liste
        for (int i = 0; i < maListe.length; i++) {
            System.out.println(maListe[i]);
        }
    }

}
